#!/bin/sh

export PATH="$PATH:$HOME/.scripts/bin"
export ENV="$HOME/.config/shell/dashrc"
export ISHELL="/usr/bin/fish"

export EDITOR=nvim
