#!/usr/bin/fish

set --export FISH 1
set -U fish_greeting

fish_vi_key_bindings
