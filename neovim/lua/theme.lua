if vim.env.TERM == "alacritty" or vim.env.TERMGUICOLORS then
	vim.opt.termguicolors = true
end


vim.cmd [[
	colorscheme vscode

	hi LineNr ctermfg=black ctermbg=grey
	hi LineNrBelow ctermbg=None
	hi LineNrAbove ctermbg=None
]]


