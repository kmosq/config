local tsc = require "nvim-treesitter.configs"

tsc.setup({
	ensure_installed = { "c", "lua" },
	sync_install = false,
	auto_install = true,
	highlight = {
		enable = true
	}
})
