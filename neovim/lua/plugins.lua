local packer_bootstrap = require("packer_bootstrap")
packer = require("packer")

packer.startup(function(use)
	use "wbthomason/packer.nvim"

	use "nvim-treesitter/nvim-treesitter"

	use "Mofiqul/vscode.nvim"
	
	if packer_bootstrap then
		packer.sync()
	end
end)
