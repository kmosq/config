local nnoremap = {
	{"<C-n>", ":bn<CR>"},
	{"<C-p>", ":bp<CR>"}
}

for _, bind in ipairs(nnoremap) do
	vim.api.nvim_set_keymap("n", bind[1], bind[2], { noremap = true })
end
