require "plugins"

vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.expandtab = false
vim.opt.nu = true
vim.opt.rnu = true

require "commands"
require "keys"
require "theme"

require "treesitter"
require "misc"
